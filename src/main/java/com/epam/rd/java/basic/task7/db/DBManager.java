package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    private static DBManager instance;
    private static Connection con;

    public static synchronized DBManager getInstance() {
        if (instance == null) instance = new DBManager();
        return instance;
    }

    private DBManager() {
        getCon();
        try (Statement statement = con.createStatement()) {
//            statement.executeUpdate("DELETE FROM users");
//            statement.executeUpdate("INSERT INTO users VALUES(DEFAULT,'ivanov')");
//            statement.executeUpdate("DELETE FROM teams");
//            statement.executeUpdate("INSERT INTO teams VALUES(DEFAULT,'teamA')");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                closeCon();
            } catch (DBException e) {
                e.printStackTrace();
            }
        }
    }
    //додав зчитування шляху до бази з app.properties
    private static void getCon() {
        try {
            Properties properties = loadProperties();
            String url = properties.getProperty("connection.url");
            con = DriverManager.getConnection(url);
        } catch (SQLException | IOException e) {
            try {
                throw new DBException(e.getMessage(),e);
            } catch (DBException ex) {
                ex.printStackTrace();
            }
        }
    }

    private static void closeCon() throws DBException {
        try {
            con.close();
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
    }

    public List<User> findAllUsers() throws DBException {
        getCon();
        List<User> users = new ArrayList<>();
        try (PreparedStatement st = con.prepareStatement("SELECT * FROM users")) {
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("id"));
                user.setLogin(rs.getString("login"));
                users.add(user);
            }
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        } finally {
            closeCon();
        }
        return users;
    }

    //додав зчитування id що повертає база
    public boolean insertUser(User user) throws DBException {
        if (user == null) return false;
        getCon();
        try (PreparedStatement st = con.prepareStatement("INSERT INTO users VALUES(DEFAULT,?)", Statement.RETURN_GENERATED_KEYS)) {
            st.setString(1, user.getLogin());
            boolean result = st.executeUpdate() != 0;

            ResultSet resultSet = st.getGeneratedKeys();
            if(resultSet.next()) user.setId(resultSet.getInt(1));
            return result;
        } catch (SQLException e) {
            return false;
        } finally {
            closeCon();
        }
    }

    public boolean deleteUsers(User... users) throws DBException {
        getCon();
        try (Statement st = con.createStatement()) {
            for (var n : users) {
                st.executeUpdate("DELETE FROM users WHERE login='" + n.getLogin() + "'");
            }
        } catch (SQLException e) {
            return false;
        } finally {
            closeCon();
        }
        return true;
    }

    public User getUser(String login) throws DBException {
        getCon();
        User user = new User();
        try (Statement st = con.createStatement()) {
            ResultSet rs = st.executeQuery("SELECT id FROM users WHERE login='" + login + "'");
            if (rs.next()) {
                user.setLogin(login);
                user.setId(rs.getInt("id"));
            }
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        getCon();
        Team team = new Team();
        try (Statement st = con.createStatement()) {
            ResultSet rs = st.executeQuery("SELECT id FROM teams WHERE name='" + name + "'");
            if (rs.next()) {
                team.setName(name);
                team.setId(rs.getInt("id"));
            }
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        getCon();
        List<Team> teams = new ArrayList<>();
        try (PreparedStatement st = con.prepareStatement("SELECT * FROM teams")) {
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Team team = new Team();
                team.setId(rs.getInt("id"));
                team.setName(rs.getString("name"));
                teams.add(team);
            }
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        } finally {
            closeCon();
        }
        return teams;
    }

    //додав зчитування id що повертає база
    public boolean insertTeam(Team team) throws DBException {
        getCon();
        if (team == null) return false;
        try (PreparedStatement st = con.prepareStatement("INSERT INTO teams VALUES(DEFAULT,?)", Statement.RETURN_GENERATED_KEYS)) {
            st.setString(1, team.getName());

            boolean result = st.executeUpdate() != 0;
            ResultSet resultSet = st.getGeneratedKeys();
            if(resultSet.next()) team.setId(resultSet.getInt(1));

            return result;
        } catch (SQLException e) {
            return false;
        } finally {
            closeCon();
        }
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        getCon();
        try (Statement st = con.createStatement()) {
            con.setAutoCommit(false);
            for (Team team : teams) {
                st.executeUpdate("INSERT INTO users_teams VALUES(" + user.getId() + "," + team.getId() + ")");
            }
        } catch (SQLException e) {
            try {
                con.rollback();
                throw new DBException(e.getMessage(),e);
            } catch (SQLException ex) {
                throw new DBException(e.getMessage(),e);
            }
        } finally {
            try {
                con.commit();
            } catch (SQLException e) {
                throw new DBException(e.getMessage(),e);
            }
            closeCon();
        }
        return true;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        getCon();
        List<Team> teams = new ArrayList<>();
        try (Statement st = con.createStatement()) {
            ResultSet rs = st.executeQuery("SELECT id,name FROM teams JOIN users_teams ON " +
                    "teams.id = users_teams.team_id WHERE users_teams.user_id=" + user.getId());
            while (rs.next()) {
                Team team = new Team();
                team.setName(rs.getString("name"));
                team.setId(rs.getInt("id"));
                teams.add(team);
            }
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        } finally {
            closeCon();
        }
        return teams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        getCon();
        try (Statement st = con.createStatement()) {
            st.executeUpdate("DELETE FROM teams WHERE id=" + team.getId());
            st.executeUpdate("DELETE FROM users_teams WHERE team_id=" + team.getId());
        } catch (SQLException e) {
            return false;
        } finally {
            closeCon();
        }
        return true;
    }

    public boolean updateTeam(Team team) throws DBException {
        getCon();
        try (PreparedStatement st = con.prepareStatement("UPDATE teams SET name = ? WHERE id=?")) {
            st.setString(1, team.getName());
            st.setInt(2, team.getId());
            st.executeUpdate();

        } catch (SQLException e) {
            return false;
        }finally {
            closeCon();
        }
        return true;
    }
    //додав для читання пропертіс
    private static Properties loadProperties() throws IOException {
        Properties properties = new Properties();

        try (InputStream inputStream = new FileInputStream("app.properties")) {
            properties.load(inputStream);
        }

        return properties;
    }

}